.PHONY: help

SHELL := /bin/bash
PATH := /usr/local/bin:/usr/bin:/bin:/usr/X11/bin:/usr/sbin:/sbin

.DEFAULT_GOAL := help

c: ## compile code
c: clean
	mvn -e scala:compile

tc: ## compile test code
	mvn -e scala:testCompile

cc: ## compile code continuously
cc: clean
	mvn scala:cc  -Dfsc=false

build: ## build
build: clean
	mvn -e scala:compile

clean: ## clean
	mvn clean

run: ## run $MAINCLASS, e.g.: make run MAINCLASS=com.foo.Main
	mvn scala:run -DmainClass=${MAINCLASS}

console: ## open a scala console (repl)
	mvn scala:console

DEV_ID ?= 1
DOCKER_ID ?= kpipes_dev-scala_${DEV_ID}
EXEC ?= /bin/bash

dev: ## open a shell in a docker context
	docker exec --tty --interactive ${DOCKER_ID} ${EXEC}

dev1:; make dev DEV_ID=1
dev2:; make dev DEV_ID=2
dev3:; make dev DEV_ID=3
dev4:; make dev DEV_ID=4

KAFKA = $(shell docker inspect kafka_kafka_1|jq ".[0].NetworkSettings.Networks.kafka_default.IPAddress" | sed 's/"//g')
ZK = $(shell docker inspect kafka_zookeeper_1|jq ".[0].NetworkSettings.Networks.kafka_default.IPAddress" | sed 's/"//g')

url-prod: ## start url producder
	mvn scala:run -DmainClass=com.conductor.app.UrlProducer

url-cons: ## start url consumer
	mvn scala:run -DmainClass=com.conductor.app.UrlConsumer

url-msg-cons: ## start url-msg consumer
	mvn scala:run -DmainClass=com.conductor.app.UrlMsgConsumer

req-msg-cons: ## start url-msg consumer
	mvn scala:run -DmainClass=com.conductor.app.ReqMsgConsumer

kafka-cons: ## start kafke-events consumer
	mvn scala:run -DmainClass=com.conductor.app.KevConsumer

kafka-prod: ## start kafke-events producer
	mvn scala:run -DmainClass=com.conductor.app.KevProducer

CONTAINERS = cassandra dev-scala  influxdb kafka zookeeper
docker-containers: ## make docker containers
	for i in ${CONTAINERS}; do (cd lib/docker/$$i && make); done

docker-clean: ## make docker containers
	for i in ${CONTAINERS}; do (docker rmi $$i); done

cluster: ## start cluster
	cd lib/cluster/kpipes && make $@

# @todo: handle case when "docker ps -a -q"
cluster-clean: ## clean cluster
	cd lib/cluster/kpipes && make $@

help: ## help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) \
		| awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-16s\033[0m %s\n", $$1, $$2}'

# install project dependencies
init: ## install dependencies and init. maven proj.
init: install-java install-wget install-maven init-pom

install-java: ## install java
	@if [ ! type java >/dev/null 2>&1 ]; then sudo make -f lib/docker/mk/java.mk; fi

install-maven: ## install maven
	@if [ ! type mvn >/dev/null 2>&1 ]; then sudo make -f lib/docker/mk/maven.mk; fi

install-wget: ## install wget
	@if [ ! type wget >/dev/null 2>&1 ]; then sudo make -f lib/docker/mk/wget.mk; fi

install-jq: ## install jq--json query tool
	@if [ ! type jq >/dev/null 2>&1 ]; then sudo make -f lib/docker/mk/jq.mk; fi

init-pom: ## generate the initial project pom.xml
	@if [ ! -f pom.xml ]; then \
		mvn archetype:generate -B \
		-DarchetypeGroupId=net.alchim31.maven \
		-DarchetypeArtifactId=scala-archetype-simple \
		-DarchetypeVersion=1.6 \
		-DgroupId=com.conductor.kpipe \
		-DartifactId=kpipe \
		-Dversion=0.1-SNAPSHOT \
		-Dpackage=com.conductor.kpipes; \
		mv -f kpipe/* . && rm -rf kpipes; \
	fi

GIT_SUBMODULE = git@gitlab.com:metaml/docker
add-submodule: ## add related git submodule
	[ -d lib ] || mkdir -p lib
	if [ ! -d lib/docker ]; then \
		(git rm -r --cached lib/docker || exit 0); \
		git submodule add --force ${GIT_SUBMODULE} lib/docker; \
	fi
