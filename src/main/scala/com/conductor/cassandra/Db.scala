package com.conductor.cassandra.Db

import com.conductor.cassandra.db.content.ConcreteContents
import com.websudos.phantom.dsl._
import scala.collection.JavaConversions._
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


object Cassandra {
  val hosts = Seq("cassandra")
  val connector = ContactPoints(hosts).keySpace("content")
}

class ContentsDb(val keyspace: KeySpaceDef) extends Database(keyspace) {
  object contents extends ConcreteContents with keyspace.Connector
}

object ContentsDb extends ContentsDb(Cassandra.connector) {
  Await.result(ContentsDb.autocreate().future(), 8.seconds)
}
