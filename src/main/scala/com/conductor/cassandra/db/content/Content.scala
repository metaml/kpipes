package com.conductor.cassandra.db.content

import com.websudos.phantom.dsl._
import scala.collection.JavaConversions._
import scala.concurrent.Future

case class Content (id: String, url: String, reqMsg: String, content: String, timestamp: DateTime)

class Contents extends CassandraTable[ConcreteContents, Content] {
  object id extends StringColumn(this) with PartitionKey[String]
  object url extends StringColumn(this)
  object reqMsg extends StringColumn(this)
  object content extends StringColumn(this)
  object timestamp extends DateTimeColumn(this)

  def fromRow(row: Row): Content = Content(id(row), url(row), reqMsg(row), content(row), timestamp(row))
}

abstract class ConcreteContents extends Contents with RootConnector {
  def store(c: Content): Future[ResultSet] = insert
    .value(_.id, c.id)
    .value(_.url, c.url)
    .value(_.reqMsg, c.reqMsg)
    .value(_.content, c.content)
    .value(_.timestamp, c.timestamp)
    .consistencyLevel_=(ConsistencyLevel.ALL)
    .future()

  def getById(id: String): Future[Option[Content]] = select.where(_.id eqs id).one()
}
