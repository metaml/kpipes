package com.conductor.kafka

import com.google.common.io.Resources
import java.lang.Long
import java.util.{Calendar, Properties}
import org.apache.kafka.clients.consumer.{Consumer, KafkaConsumer, ConsumerRecord}
import org.apache.kafka.clients.producer.{Producer, KafkaProducer, ProducerRecord}
import org.apache.kafka.common.errors.WakeupException
import scala.collection.JavaConversions._

object Queue {
  def mkProp(props: String) = {
    val res = Resources.getResource(props).openStream()
    val prop = new Properties()
    prop.load(res)
    prop
  }

  def mkWriter(producer: Producer[String, String]) = new Writer(producer)
  def mkPoller(consumer: Consumer[String, String], h: ConsumerRecord[String, String] => Unit) =
    new Poller(consumer, h)

  def mkConsumer(prop: Properties) = new KafkaConsumer[String, String](prop)
  def mkProducer(prop: Properties) = new KafkaProducer[String, String](prop)

  def mkRecord(topic: String, key: String, valu: String) = new ProducerRecord[String, String](topic, key, valu)
}

class Writer(producer: Producer[String, String]) {
  def put(rec: ProducerRecord[String, String]): Unit = {
    try {
      producer.send(rec)
    } catch {
      case x: Exception =>
        val trace = x.getStackTrace().mkString("\n")
        println(s"writer exception:\n$trace")
    }
  }
}

class Poller(consumer: Consumer[String, String], handler: ConsumerRecord[String, String] => Unit) {
  def poll(topics: List[String]): Unit = {
    try {
      consumer.subscribe(topics)
      while (true) {
        val recs = consumer.poll(Long.MAX_VALUE)
        for (rec <- recs)
          handler(rec)
      }
    } catch {
      case x: WakeupException =>
        val t = x.getStackTrace()
        println(s"poller exception: $t")
      case x: Exception =>
        val t = x.getStackTrace()
        println(s"poller exception: $t")
    } finally {
      consumer.close()
    }
  }
}
