package com.conductor.kafka.topic

import argonaut._, Argonaut._
import com.conductor.util.Msg
import java.util.UUID

object Topic {
  val url = "url"
  val urlMsg = "urlMsg"
  val reqMsg = "reqMsg"
  val httpGetMsg = "httpGetMsg"
  val kafkEventMsg = "kafkaEventMsg"
  val errorEventMsg = "errorEventMsg"
}

case class UrlMsg(globalId: String, clientId: String, url: String)

case class ReqMsg(globalId: String, clientId: String, url: String, headers: List[String], normUrl: String)

case class KafkaEventMsg(globalId: String, clientId: String, topic: String, direction: String, payload: String, timestamp: Long)

case class HttpGetEventMsg(globalId: String, clientId: String, url: String, headers: List[String], startTime: Long, endTime: Long, timestamp: Long)

// globalId may be wishful thinking--let's see
case class ErrorEventMsg(source: String, error: String, errorType: String, stacktrace: List[String], payload: String, anId: String, timestamp: Long)


object Codec {
  implicit def urlMsg2Json: CodecJson[UrlMsg] =
    casecodec3(UrlMsg.apply, UrlMsg.unapply)("globalId", "clientId", "url")

  implicit def reqMsg2Json: CodecJson[ReqMsg] =
    casecodec5(ReqMsg.apply, ReqMsg.unapply)("globalId", "clientId", "url", "headers", "normUrl")

  implicit def kafkaEventMsg2Json: CodecJson[KafkaEventMsg] =
    casecodec6(KafkaEventMsg.apply, KafkaEventMsg.unapply)("globalId", "clientId", "topic", "direction", "payload", "timestamp")

  implicit def httpGetEventMsg2Json: CodecJson[HttpGetEventMsg] =
    casecodec7(HttpGetEventMsg.apply, HttpGetEventMsg.unapply)("globalId", "clientId", "url", "headers", "startTime", "endTime", "timestamp")

  implicit def errorEventMsg2Json: CodecJson[ErrorEventMsg] =
    casecodec7(ErrorEventMsg.apply, ErrorEventMsg.unapply)("source", "error", "errorType", "stacktrace", "payload", "anId", "timestamp")

  // for now use UUID
  def url2UrlMsg(url: String): UrlMsg = UrlMsg(UUID.randomUUID().toString, Msg.clientId(url), url)
}
