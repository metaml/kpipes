package com.conductor.app

import argonaut._, Argonaut._
import com.conductor.influxdb.KafkaEvents
import com.conductor.kafka.topic.KafkaEventMsg
import com.conductor.kafka.topic.Codec.kafkaEventMsg2Json // implicit
import com.conductor.actor.Router
import com.conductor.util.Kafka.consumer
import org.apache.kafka.clients.consumer.ConsumerRecord

object KevConsumer {
  def main(args: Array[String]): Unit = consumer(KafkaEvents.series, addEvent, "kafka-events")

  def addEvent(rec: ConsumerRecord[String, String]): Unit = {
    val kev: Option[KafkaEventMsg] = rec.value.decodeOption[KafkaEventMsg]
    kev match {
      case Some(kevMsg) => Router.influxDb ! kevMsg
      case None => println("error [parsing]:" + rec.value)
    }
  }
}
