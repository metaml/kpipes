package com.conductor.app

import com.conductor.actor.Router
import com.conductor.kafka.topic.Topic
import com.conductor.util.Kafka.consumer
import org.apache.kafka.clients.consumer.ConsumerRecord

object UrlConsumer {
  def main(args: Array[String]): Unit = consumer(Topic.url, putUrl, group=Topic.url)
  def putUrl(rec: ConsumerRecord[String, String]): Unit = Router.urlTopic ! rec.value
}
