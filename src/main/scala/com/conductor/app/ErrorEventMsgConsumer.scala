package com.conductor.app

import argonaut._, Argonaut._
import com.conductor.actor.Router
import com.conductor.influxdb.KafkaEvents.{Egress}
import com.conductor.kafka.topic.Codec._
import com.conductor.kafka.topic.{ErrorEventMsg, UrlMsg, Topic}
import com.conductor.util.Kafka.consumer
import org.apache.kafka.clients.consumer.ConsumerRecord

object ErrorEventConsumer {
  def main(args: Array[String]): Unit = consumer(Topic.urlMsg, errorMsg, "error-msg")

  def errorMsg(rec: ConsumerRecord[String, String]): Unit = {
    val msg = Parse.decodeOption[ErrorEventMsg](rec.value)
    msg match {
      case Some(err) => Router.influxDb ! err
      case None => println("ErrorEventConsumer: None")
    }
  }
}
