package com.conductor.app

import argonaut._, Argonaut._
import com.conductor.actor.Router
import com.conductor.influxdb.KafkaEvents.{Egress}
import com.conductor.kafka.topic.Codec._
import com.conductor.kafka.topic.{KafkaEventMsg, UrlMsg, Topic}
import com.conductor.util.Kafka.consumer
import org.apache.kafka.clients.consumer.ConsumerRecord

object UrlMsgConsumer {
  def main(args: Array[String]): Unit = consumer(Topic.urlMsg, urlMsg, "url-msg")

  def urlMsg(rec: ConsumerRecord[String, String]): Unit = {
    val m = Parse.decodeOption[UrlMsg](rec.value)
    m match {
      case Some(msg) => {
        Router.influxDb ! KafkaEventMsg(msg.globalId, msg.clientId, Topic.urlMsg, Egress.dir, rec.value, System.currentTimeMillis)
        Router.urlMsgTopic ! msg
      }
      case None => println("UrlMsgConsumer: None")
    }
  }
}
