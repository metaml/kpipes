package com.conductor.app

import java.util.Calendar
import com.conductor.kafka.Queue
import com.conductor.kafka.topic.Topic

object UrlProducer {
  def main(args: Array[String]): Unit = {
    val producer = Queue.mkProducer(Queue.mkProp("producer.props"))
    try {
      val w = Queue.mkWriter(producer)
      for ((url, i) <- urls().view.zipWithIndex) {
        println(i + ": " + url)
        w.put(Queue.mkRecord(Topic.url, key=i.toString(), valu=url))
      }
    } catch {
      case x: Exception => println("exception: " + x)
    } finally {
      producer.close()
    }
  }

  def urls(): List[String] = List("http://www.shutterstock.com"
    , "http://www.reddit.com"
    , "http://www.duckduckgo.com"
    , "http://www.NYTimes.COM"
    , "http://Bing.COM"
    , "http://www.Amazon.COM"
    , "bogus://mome.raths.outgrabe/beware/the/jabberwock"
    , "http://Sir.Tristram.violer/damores")
}
