package com.conductor.app

import argonaut._, Argonaut._
import com.conductor.influxdb.KafkaEvents
import com.conductor.influxdb.KafkaEvents.{Ingress, Egress}
import com.conductor.kafka.Queue
import com.conductor.kafka.topic.Codec.kafkaEventMsg2Json
import com.conductor.kafka.topic.KafkaEventMsg
import scala.collection.mutable.MutableList

object KevProducer {
  def main(args: Array[String]): Unit = {
    val producer = Queue.mkProducer(Queue.mkProp("producer.props"))
    try {
      val w = Queue.mkWriter(producer)
      for (msg <- events) {
        println(msg)
        val json = msg.asJson.toString
        w.put(Queue.mkRecord(KafkaEvents.series, msg.globalId, json))
        producer.flush()
      }
    } catch {
      case x: Exception => println("exception: " + x)
    } finally {
      producer.close()
    }
  }

  def urls(): List[String] = List("") // remove

  def events(): List[KafkaEventMsg] = {
    val es = MutableList[KafkaEventMsg]()
    es += KafkaEventMsg("1", "url", "1", Ingress.dir, "http://google.com", System.currentTimeMillis)
    es += KafkaEventMsg("2", "url", "2", Ingress.dir, "http://reddit.com", System.currentTimeMillis)
    es += KafkaEventMsg("2", "url", "2", Ingress.dir, "http://duckduckgo.com", System.currentTimeMillis)
    es.toList
  }
}
