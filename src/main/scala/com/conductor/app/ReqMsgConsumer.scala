package com.conductor.app

import argonaut._, Argonaut._
import com.conductor.actor.Router
import com.conductor.influxdb.KafkaEvents.{Egress}
import com.conductor.kafka.topic.Codec._
import com.conductor.kafka.topic.{KafkaEventMsg, ReqMsg, Topic}
import com.conductor.util.Kafka.consumer
import org.apache.kafka.clients.consumer.ConsumerRecord

object ReqMsgConsumer {
  def main(args: Array[String]): Unit = consumer(Topic.reqMsg, put, "reg-msg")

  def put(rec: ConsumerRecord[String, String]): Unit = {
    val m = Parse.decodeOption[ReqMsg](rec.value)
    println("ReqMsgConsumer: " + rec.value)
    m match {
      case Some(msg) => {
        Router.influxDb ! KafkaEventMsg(msg.globalId, msg.clientId, Topic.reqMsg, Egress.dir, rec.value, System.currentTimeMillis)
        Router.httpGet ! msg
      }
      case None => println("ReqMsgConsumer: None")
    }
  }
}
