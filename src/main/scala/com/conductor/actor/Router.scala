package com.conductor.actor

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.routing.RoundRobinPool

// local to a JVM for this POC: much better abstraction than threads
object Router {
  val asys = ActorSystem("blaberus")

  val urlTopic = asys.actorOf(Props[UrlActor].withRouter(RoundRobinPool(4)), name="urlTopicPool")
  val urlMsgTopic = asys.actorOf(Props[UrlMsgActor].withRouter(RoundRobinPool(4)), name="urlMsgTopicPool")
  val reqMsgTopic = asys.actorOf(Props[ReqMsgActor].withRouter(RoundRobinPool(4)), name="reqMsgTopicPool")

  val httpGet = asys.actorOf(Props[HttpGetActor].withRouter(RoundRobinPool(4)), name="httpGetPool")
  val cassandraDb = asys.actorOf(Props[CassandraDbActor].withRouter(RoundRobinPool(4)), name="cassandraDbTopicPool")
  val influxDb = asys.actorOf(Props[InfluxDbActor].withRouter(RoundRobinPool(4)), name="influxDbPool")
}
