package com.conductor.actor

import akka.actor.{Actor, ActorLogging}
import com.conductor.influxdb.{ErrorEvents, HttpGetEvents, KafkaEvents}
import com.conductor.kafka.topic.{ErrorEventMsg, HttpGetEventMsg, KafkaEventMsg}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

class InfluxDbActor extends Actor with ActorLogging {
  def receive: Receive = {
    case m: KafkaEventMsg => {
      KafkaEvents.appendMsg(m) onComplete {
        case Success(r) => println(self.path.name + ": success=" + r + "; msg=" + m)
        case Failure(r) => println(self.path.name + ": failure=" + r)
      }
    }
    case m: HttpGetEventMsg => {
      try {
        HttpGetEvents.appendMsg(m)
      } catch {
        case x: Exception => println(self.path.name + ": " + x)
      }
    }
    case m: ErrorEventMsg => {
      try {
        println(self.path.toString + ": m" + m)
        ErrorEvents.appendMsg(m)
      } catch {
        case x: Exception => println(self.path.name + ": " + x)
      }
    }
    case x => println(self.path.name + ": " + x)
  }
}
