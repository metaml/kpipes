package com.conductor.actor

import akka.actor.{Actor, ActorLogging}
import argonaut._, Argonaut._
import com.conductor.cassandra.db.content.{Content, Contents}
import com.conductor.kafka.Queue
import com.conductor.kafka.topic.Codec._
import com.conductor.kafka.topic.{ReqMsg, HttpGetEventMsg}
import com.conductor.util.{FutureWithTimer, ResultWithTime, FailureWithTime, Kafka}
import dispatch._, Defaults._
import org.joda.time.DateTime
import scala.util.{Failure, Success}

class HttpGetActor extends Actor with ActorLogging {
  def receive: Receive = {
    case msg: ReqMsg => {
      val req = url(msg.url)
      lazy val getFut: Future[String] = Http.configure(_.setFollowRedirect(true))(req OK as.String)
      val futRes = FutureWithTimer(getFut)
      futRes onComplete {
        case Success(res) => {
          res match {
            case ResultWithTime(rec, start, end) => {
              Router.cassandraDb ! Content(msg.globalId, msg.url, msg.asJson.toString, rec.value, DateTime.now())
              Router.influxDb ! HttpGetEventMsg(msg.globalId, msg.clientId, msg.url, msg.headers, start, end, System.currentTimeMillis)
            }
          }
        }
        case Failure(x) => {
          println(self.path.name + " failure: " + x)
          Router.influxDb ! Kafka.mkErrorEventMsg(self.path.toString, x, msg.url, msg.globalId)
        }
      }
    }
    case x => println(self.path.name + ": " + x)
  }
}
