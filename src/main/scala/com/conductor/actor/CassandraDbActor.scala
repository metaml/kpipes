package com.conductor.actor

import akka.actor.{Actor, ActorLogging}
import com.conductor.cassandra.Db.ContentsDb
import com.conductor.cassandra.db.content.{Content, Contents}
import dispatch._, Defaults._

// @todo: handle error cases
class CassandraDbActor extends Actor with ActorLogging {
  def receive: Receive = {
    case content: Content => ContentsDb.contents.store(content)
    case x => println(self.path.name + ": " + x)
  }
}

