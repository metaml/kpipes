package com.conductor.actor

import akka.actor.{Actor, ActorLogging}
import argonaut._, Argonaut._
import com.conductor.influxdb.KafkaEvents.{Egress, Ingress}
import com.conductor.kafka.topic.{KafkaEventMsg, ReqMsg, UrlMsg, Topic}
import com.conductor.kafka.Queue
import com.conductor.kafka.topic.Codec._
import com.conductor.util.Msg
import org.apache.kafka.clients.producer.Producer

// URL: String -> url: UrlMsg
class UrlMsgActor extends Actor with ActorLogging {
  def receive: Receive = {
    case m: UrlMsg => {
      val req = ReqMsg(m.globalId, m.clientId, m.url, List(""), Msg.normalize(m.url))
      val json = req.asJson.toString
      try {
        println(self.path.name + ": urlMsg=" + m + "; req=" + req)
        Queue.mkWriter(producer).put(Queue.mkRecord(Topic.reqMsg, req.globalId, json))
        Router.influxDb ! KafkaEventMsg(req.globalId, req.clientId, Topic.reqMsg, Ingress.dir, json, System.currentTimeMillis())
      } catch {
        case x: Exception => println(self.path.name + ": exception=" + x)
      } finally {
        producer.flush
      }
    }
    case x => println(self.path.name + ": " + x)
  }

  override def preStart() = if (null == producer) producer = Queue.mkProducer(Queue.mkProp("producer.props"))
  override def postStop() = if (producer != null) producer.close()

  var producer: Producer[String, String] = null
}
