package com.conductor.actor

import akka.actor.{Actor, ActorLogging}
import argonaut._, Argonaut._
import com.conductor.kafka.Queue
import com.conductor.kafka.topic.Codec._
import com.conductor.kafka.topic.{ReqMsg, Topic, UrlMsg}
import com.conductor.util.Msg
import org.apache.kafka.clients.producer.Producer

class ReqMsgActor extends Actor with ActorLogging {
  def receive: Receive = {
    case m: ReqMsg => {
      try {
        println(self.path.name + ": m=" + m)
      } catch {
        case x: Exception => println(self.path.name + ": exception=" + x)
      } finally {
        producer.flush
      }
    }
    case x => println(self.path.name + ": " + x)
  }

  override def preStart() = if (null == producer) producer = Queue.mkProducer(Queue.mkProp("producer.props"))
  override def postStop() = if (producer != null) producer.close()

  var producer: Producer[String, String] = null
}
