package com.conductor.actor

import akka.actor.{Actor, ActorLogging}
import argonaut._, Argonaut._
import com.conductor.influxdb.KafkaEvents.{Egress, Ingress}
import com.conductor.influxdb.ErrorEvents
import com.conductor.kafka.Queue
import com.conductor.kafka.topic.Codec._
import com.conductor.kafka.topic.{Topic, UrlMsg, KafkaEventMsg}
import com.conductor.util.{Msg, Kafka}
import org.apache.kafka.clients.producer.Producer

// URL: String -> url: UrlMsg
class UrlActor extends Actor with ActorLogging {
  def receive: Receive = {
    case url: String => {
      try {
        val m = UrlMsg(Msg.globalId(url), Msg.clientId(url), url)
        val json = m.asJson.toString
        Queue.mkWriter(producer).put(Queue.mkRecord(Topic.urlMsg, m.globalId, json))
        Router.influxDb ! KafkaEventMsg(m.globalId, m.clientId, Topic.urlMsg, Ingress.dir, json, System.currentTimeMillis())
        println(self.path.name + " url=" + url)
      } catch {
        case x: Exception => Router.influxDb ! Kafka.mkErrorEventMsg(self.path.toString, x, url, Msg.globalId(url))
      } finally {
        producer.flush
      }
    }
    case x => println(self.path.name + ": " + x)
  }

  override def preStart() = if (null == producer) producer = Queue.mkProducer(Queue.mkProp("producer.props"))
  override def postStop() = if (producer != null) producer.close()

  var producer: Producer[String, String] = null
}
