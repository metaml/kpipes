package com.conductor.influxdb

import argonaut._, Argonaut._
import com.conductor.kafka.topic.ErrorEventMsg

object ErrorEvents {
  val series = "errorEvent"
  private val db = TsDb.influxDb.selectDatabase(series)
  db.create

  def appendMsg(m: ErrorEventMsg) = append(m.source, m.error, m.errorType, m.stacktrace, m.payload, m.anId, m.timestamp)

  def append(source: String, error: String, errorType: String, stacktrace:List[String], payload: String, anId: String, timestamp: Long) = {
    val tags = List(("source", source), ("error", error), ("errorType", errorType)
      , ("stacktrace", stacktrace.map(_.trim).mkString(";"))
      , ("payload", payload)
      , ("anId", anId))
    val fields = List(("ignore", 0.0))
    InfluxDb.add(db, series, timestamp, tags, fields)
  }
}
