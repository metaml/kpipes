package com.conductor.influxdb

import com.conductor.kafka.topic.HttpGetEventMsg

object HttpGetEvents {
  val series = "httpGetEvent"
  private val db = TsDb.influxDb.selectDatabase(series)
  db.create

  def appendMsg(m: HttpGetEventMsg) = append(m.globalId, m.clientId, m.startTime, m.endTime, m.url, m.headers, m.timestamp)

  def append(globalId: String, clientId: String, startTime: Long, endTime: Long, url: String, headers: List[String], timestamp: Long) {
    val tags = List(("globalId", globalId), ("url", url))
    val fields = List(("startTime", startTime.toDouble), ("endTime", endTime.toDouble))
    // @todo: "topic" isn't a good name for this
    InfluxDb.add(db, series, timestamp, tags, fields)
  }
}
