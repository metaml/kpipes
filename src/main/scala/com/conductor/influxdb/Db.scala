package com.conductor.influxdb

import com.paulgoldbaum.influxdbclient.{Database, InfluxDB, Point, DoubleField, Tag}
import com.paulgoldbaum.influxdbclient.Parameter.{Consistency, Precision}
import scala.concurrent.Future

object TsDb {
  val host = "influxdb"
  val port = 8086
  val influxDb = InfluxDB.connect(host, port)
}

object InfluxDb {
  // timestamp in ms
  def add(db: Database, series: String, timestamp: Long, tags: List[(String, String)], fields: List[(String, Double)]): Future[Boolean] = {
    val ts = if (tags != null) tags.map(t => Tag(t._1, t._2)) else List()
    val fs = if (fields != null) fields.map(t => DoubleField(t._1, t._2)) else List()
    val point = Point(series, timestamp, ts, fs)
    db.write(point, precision=Precision.MILLISECONDS, consistency=Consistency.ALL)
  }
}
