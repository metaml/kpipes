package com.conductor.influxdb

import argonaut._, Argonaut._
import com.conductor.kafka.topic.KafkaEventMsg
import com.conductor.kafka.topic.Codec.kafkaEventMsg2Json
import scala.concurrent.Future

object KafkaEvents {
  sealed trait Direction { def dir: String }
  case object Ingress extends Direction { val dir = "ingress"}
  case object Egress extends Direction { val dir = "egress"}

  val series = "kafkaEvent"
  private val db = TsDb.influxDb.selectDatabase(series)
  db.create

  def appendMsg(m: KafkaEventMsg) =
    append(m.globalId
      , m.clientId
      , m.topic
      , if (m.direction == Ingress.dir) Ingress else Egress
      , m.payload
      , m.timestamp)

  def append(globalId: String, clientId: String, qtopic: String, dir: Direction, payload: String, timestamp: Long): Future[Boolean] = {
    val tags = List(("globalId", globalId), ("clientId", clientId), ("topic", qtopic), ("direction", dir.dir), ("payload", payload))
    val fields = List(("ignore", 0.0)) // @todo: hack should not be needed but makes influxdb happy
    InfluxDb.add(db, series, timestamp, tags, fields)
  }
}
