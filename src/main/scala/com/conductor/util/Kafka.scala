package com.conductor.util

import com.conductor.kafka.Queue
import com.conductor.kafka.topic.ErrorEventMsg
import java.net.URL
import java.util.UUID
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.errors.WakeupException

object Kafka {
  def consumer(topic: String, consume: ConsumerRecord[String, String] => Unit, group: String) = {
    val consumer = Queue.mkConsumer(mkProp("consumer.props", group))
    try {
      val p = Queue.mkPoller(consumer, consume)
      p.poll(List(topic))
    } catch {
      case x: WakeupException => println(x)
      case x: Exception => println(x)
    } finally {
      consumer.close()
    }
  }

  // "name" refers to a resource, e.g.: consumer.prop in ./resources/.
  def mkProp(name: String, group: String) = {
    val prop = Queue.mkProp(name)
    prop.put("group.id", group)
    prop
  }

  def mkErrorEventMsg(source: String, x: Throwable, payload: String, anId: String) =
    ErrorEventMsg(source, x.getMessage, x.getClass.getName, x.getStackTrace.map{_.toString}.toList, payload
      ,anId, System.currentTimeMillis)
}

object Msg {
  def globalId(url: String) = UUID.randomUUID().toString()
  def clientId(url: String): String = {
    // easy-cheesy way to get an ID, only for POC
    val tlds = List("com", "edu", "gov", "int", "net", "org", "mil")
    val es = hostname(url).toLowerCase.split("\\.")
    es.length match {
      case 0 => if (hostname(url).length > 0) hostname(url).toLowerCase else url.toLowerCase
      case 1 => es.head
      case 2 => es.mkString(".")
      case 3 => if (tlds.contains(es.last)) es.tail.mkString(".") else es.mkString(".")
      case _ => if (tlds.contains(es.last)) es.takeRight(2).mkString(".") else es.takeRight(3).mkString(".")
    }
  }
  def normalize(url: String) = url.toLowerCase
  def hostname(url: String) = (new URL(url)) match {
    // @todo: case isn't exhaustive(?)
    case UrlProjection(proto, host, port, path) => host
  }
}

object UrlProjection {
  def unapply(url: URL) = Some((url.getProtocol
    , url.getHost
    , url.getPort
    , url.getPath))
}
