package com.conductor.util
// stolen from stackexchange
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

case class ResultWithTime[ResultType](result: ResultType, startTime: Long, endTime: Long)
case class FailureWithTime(failure: Exception, startTime: Long, endTime: Long) extends Exception(failure)

object FutureWithTimer {
  def apply[ResultType](futRes: => Future[ResultType]): Future[ResultWithTime[ResultType]] = {
    val startTime = System.currentTimeMillis
    futRes.map(r => ResultWithTime(r, startTime, System.currentTimeMillis)).recoverWith {
      case x: Exception => Future.failed(FailureWithTime(x, startTime, System.currentTimeMillis))
    }
  }
}
