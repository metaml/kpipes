### kpipes (kafka all the down) POC

0. requirement: ubuntu 15.10
1. initialize project (install dependencies): make init
2. start dev cluster: make cluster
3. to enter a docker context:
   1. make dev
   2. make dev DEV_ID=2
   3. make dev DOCKER_ID=<container> LOGNAME=root
4. issues and refactoring ideas:
   - 3 invariant types of queue processes: consumer, producer, and pipe; the latter join of a consumer and producer; take advantage of this invariant by expressing into an API or code
   - fault-tolerance, i.e., continue processing at point where a process or machine died--transaction log of some sort or samsa?

NB: 1. "make dev" will put into a dev instance that's a part of the cluster; your dev environment is there to test code with the infrastructure pieces
    2. there are two dev envs for producer/consumer testing
    3. curently using java 1.8.0_77 and docker 1.8.0_77; versions of daemons are found in lib/docker
